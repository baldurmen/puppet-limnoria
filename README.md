# limnoria

#### Table of Contents

1. [Description](#description)
2. [Setup](#setup)
    * [Setup requirements](#setup-requirements)
    * [Getting started](#getting-started)
3. [Limitations](#limitations)
4. [Reference](#reference)
5. [Development](#development)

## Description

This module installs and manages Limnoria IRC bots.

It currently only support Debian.

## Setup

### Setup Requirements

This module needs:

 * the [stdlib module](https://github.com/puppetlabs/puppetlabs-stdlib.git)

Explicit dependencies can be found in the project's metadata.json file.

### Getting started

Here is an example of a working configuration:

``` puppet
  class { 'limnoria': }

  limnoria::bot {
    'gforeman_grill_bot':
      server_fqdn    => 'irc.indymedia.org',
      server_name    => 'indymedia',
      nick           => 'gforeman_grill_bot',
      channels       => [ '#grill', '#foremanofficial' ],
      owner_info     => "gforeman's irc bot",
      sasl_password  => 'mysupersecretpassword',
      custom_options => {
        'supybot.plugins'                       => 'Web',
        'supybot.plugins.Web'                   => 'True',
        'supybot.plugins.Web.checkIgnored'      => 'True',
        'supybot.plugins.Web.public'            => 'True',
        'supybot.plugins.Web.snarfMultipleUrls' => 'True',
        'supybot.plugins.Web.snarferPrefix'     => '',
        'supybot.plugins.Web.titleSnarfer'      => 'True',
        'supybot.plugins.alwaysLoadImportant'   => 'False',
        };
    };
```

## Limitations

This module won't configure NickServ for you on the IRC server of your choice.
Should you desire to register your bot (you really should), please use the sasl
functionality.

At the moment, this module only supports Debian 10.

# Reference

The full reference documentation for this module may be found at on
[GitLab Pages][pages].

Alternatively, you may build yourself the documentation using the
`puppet strings generate` command. See the documentation for
[Puppet Strings][strings] for more information.

[pages]: https://baldurmen.gitlab.io/puppet-limnoria
[strings]: https://puppet.com/blog/using-puppet-strings-generate-great-documentation-puppet-modules

## Development

This module's development is tracked on GitLab. Please submit issues and merge
requests on the [baldurmen/limnoria][baldurmen] project page.

[baldurmen]: https://gitlab.com/baldurmen/puppet-limnoria
