# @summary configure a limnoria bot
#
# @param server_fqdn
#   The FQDN of the server the bot should connect to.
#
# @param server_name
#   The name of the server the bot should connect to.
#
# @param nick
#   The nick of the bot.
#
# @param port
#   The port on the server the bot should connect to.
#
# @param ssl
#   If the bot should connect to the server using TLS or not.
#
# @param channels
#   A list of channels the bot should connect to on the server.
#
# @param owner_info
#   Information identifying the bot's owner on the IRC server.
#
# @param sasl_password
#   The password the bot should use to register on the server's NickServ
#
# @param custom_options
#   All other options you want to pass to the configuration file.
#   
define limnoria::bot (
  Stdlib::FQDN      $server_fqdn,
  String            $server_name,
  String            $nick,
  Integer           $port,
  Boolean           $ssl,
  Array[String]     $channels,
  String            $owner_info,
  Optional[String]  $sasl_password,
  Optional[Hash]    $custom_options,
) {

  service {
    "limnoria@${name}":
      ensure     => running,
      hasrestart => true,
      hasstatus  => true,
      enable     => true,
      require    => File['/etc/systemd/system/limnoria@.service'];
  }

  # lint:ignore:140chars lint:ignore:variable_scope
  $_config = {
    'supybot.abuse.flood.command.invalid.notify'    => 'False',
    'supybot.abuse.flood.command.notify'            => 'False',
    'supybot.commands.allowShell'                   => 'False',
    'supybot.commands.nested'                       => 'False',
    'supybot.databases.users.timeoutIdentification' => '5',
    'supybot.defaultIgnore'                         => 'False',
    'supybot.directories.backup'                    => '/dev/null',
    'supybot.directories.conf'                      => "/home/${limnoria::user}/conf",
    'supybot.directories.data'                      => "/home/${limnoria::user}/data",
    'supybot.directories.data.tmp'                  => '/tmp',
    'supybot.directories.data.web'                  => "/home/${limnoria::user}/web",
    'supybot.directories.log'                       => $limnoria::log_dir,
    'supybot.flush'                                 => 'False',
    'supybot.ident'                                 => 'limnoria',
    'supybot.log.level'                             => 'WARNING',
    'supybot.log.stdout'                            => 'False',
    'supybot.networks'                              => $server_name,
    "supybot.networks.${server_name}.servers"       => "${server_fqdn}:${port}",
    "supybot.networks.${server_name}.ssl"           => $ssl,
    'supybot.nick'                                  => $nick,
    'supybot.pidFile'                               => "/home/${limnoria::user}/${nick}.pid",
    'supybot.protocols.ssl.verifyCertificates'      => 'true',
    'supybot.user'                                  => $owner_info,
    'supybot.replies.error'                         => "An error has occurred and has been logged. Please contact this bot's administrator for more information.",
    'supybot.replies.errorOwner'                    => 'An error has occurred and has been logged. Check the logs for more information.',
    'supybot.replies.genericNoCapability'           => "You're missing some capability you need. This could be because you actually possess the anti-capability for the capability that's required of you, or because the channel provides that anti-capability by default, or because the global capabilities include that anti-capability. Or, it could be because the channel or supybot.capabilities.default is set to False, meaning that no commands are allowed unless explicitly in your capabilities. Either way, you can't do what you want to do.",
    'supybot.replies.incorrectAuthentication'       => "Your hostmask doesn't match or your password is wrong.",
    'supybot.replies.noCapability'                  => "You don't have the %s capability. If you think that you should have this capability, be sure that you are identified before trying again. The 'whoami' command can tell you if you're identified.",
    'supybot.replies.noUser'                        => "I can't find %s in my user database. If you didn't give a user name, then I might not know what your user is, and you'll need to identify before this command might work.",
    'supybot.replies.notRegistered'                 => 'You must be registered to use this command. If you are already registered, you must either identify (using the identify command) or add a hostmask matching your current hostmask (using the "hostmask add" command).',
    'supybot.replies.possibleBug'                   => 'This may be a bug. If you think it is, please file a bug report at <https://github.com/ProgVal/Limnoria/issues>.',
    'supybot.replies.requiresPrivacy'               => 'That operation cannot be done in a channel.',
    'supybot.replies.success'                       => 'The operation succeeded.',
  }

  if $sasl_password {
    $_sasl_config = {
      "supybot.networks.${server_name}.sasl.username" => $nick,
      "supybot.networks.${server_name}.sasl.password" => $sasl_password,
    }
  } else {
    $_sasl_config = {}
  }

  file {
    "/home/${limnoria::user}/${name}.conf":
      ensure  => file,
      content => epp('limnoria/bot.conf.epp', {
        'channels'    => $channels,
        'config'      => deep_merge($_config, $_sasl_config, $custom_options),
        'server_name' => $server_name,
      }),
      owner   => $limnoria::user,
      group   => $limnoria::group,
      mode    => '0600',
      notify  => Service["limnoria@${name}"];
  }
  # lint:endignore
}
