# @summary create the limnoria user and manage boilerplate files
#
class limnoria::config {

  group {
    $limnoria::group:
      ensure => present,
      name   => $limnoria::group;
  }

  user {
    $limnoria::user:
      ensure     => present,
      name       => $limnoria::user,
      gid        => $limnoria::user,
      home       => "/home/${limnoria::user}",
      managehome => true,
      password   => '*',
  }

  file {
    [ $limnoria::log_dir,
      "/home/${limnoria::user}/data",
      "/home/${limnoria::user}/conf",
      "/home/${limnoria::user}/web" ]:
        ensure => directory,
        owner  => $limnoria::user,
        group  => $limnoria::group,
        mode   => '0750';

    '/etc/systemd/system/limnoria@.service':
      ensure  => present,
      content => epp('limnoria/limnoria.service.epp', {
        'user' => $limnoria::user,
      }),
      owner   => 'root',
      group   => 'root',
      mode    => '0644';
  }
}
