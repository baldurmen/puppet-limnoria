# @summary install the required packages
#
class limnoria::install {

  package {
    'limnoria':
      ensure => present;
  }
}
