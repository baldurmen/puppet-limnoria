# @summary Main class, includes all other classes.
#
# @param user
#   The unix user used to run the different bots.
#
# @param group
#   The unix group used to run the different bots.
#
# @param log_dir
#   The directory where limnoria log files should be stored.
#
class limnoria (
  String           $user,
  String           $group,
  Stdlib::Unixpath $log_dir,
) {

include ::limnoria::install
include ::limnoria::config
}
